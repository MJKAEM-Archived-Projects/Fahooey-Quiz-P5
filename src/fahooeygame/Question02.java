package fahooeygame;

import processing.core.PApplet;


/**
 * Obsolete. Now uses UTF code to do the dirty work.
 * @author Electrode on Steroids
 */
public class Question02 extends Question
{
	public Question02(PApplet Simon, String q[], String[] a, int correctNum)
	{
		super(Simon, q, a, correctNum);
	}
	public void DisplayQuestion()
	{
		GetSimon().fill(0);
		GetSimon().textSize(48);
		GetSimon().textAlign(GetSimon().CENTER);

		for(int i = GetQuestionText().length - 1; i >= 0; i--)
		{
			GetSimon().text(GetQuestionText(i), GetSimon().width / 2, 50 + (50 * i));
		}
		GetSimon().textSize(24);
		GetSimon().fill(0);
		GetSimon().text(2, GetSimon().width / 2 - 25, 50 + 25);
	}
	public void DisplayAnswers()
	{
		GetSimon().textSize(24);

		//Upper Left Box (0)
		if(GetSimon().mouseX > 20 && GetSimon().mouseX < GetSimon().width / 2 - 20)
		{
			if(GetSimon().mouseY > GetSimon().height / 2 + 5 && 
					GetSimon().mouseY < GetSimon().height * (0.75) - 20)
			{
				GetSimon().fill(30,255,255);
			}
			else
			{
				GetSimon().fill(255);
			}
		}
		else
		{
			GetSimon().fill(255);
		}
		GetSimon().rect(
				20, 
				GetSimon().height / 2 + 5, 
				GetSimon().width / 2 - 20 - 20, 
				(float)(GetSimon().height * (0.75) - 20 - (GetSimon().height / 2 + 5))
				);

		GetSimon().fill(0);
		GetSimon().text(GetAnswers(0).GetAnswer(), (int)((GetSimon().width / 2 - 20) / 2), (int)(GetSimon().height / 2 + 5) + 50);

		//Lower Left Box (1)
		if(GetSimon().mouseX > 20 && GetSimon().mouseX < GetSimon().width / 2 - 20)
		{
			if(GetSimon().mouseY > GetSimon().height * (.75) + 5 &&
					GetSimon().mouseY < GetSimon().height - 20)
			{
				GetSimon().fill(30,255,255);
			}
			else
			{
				GetSimon().fill(255);
			}
		}
		else
		{
			GetSimon().fill(255);
		}
		GetSimon().rect(
				20, 
				(float)(GetSimon().height * (.75) + 5), 
				GetSimon().width / 2 - 20 - 20, 
				(float)(GetSimon().height - 20 - (GetSimon().height * (.75) + 5))
				);
		GetSimon().fill(0);
		GetSimon().text(GetAnswers(1).GetAnswer(), (int)((GetSimon().width / 2 - 20) / 2), (int)(GetSimon().height *.75 + 5) + 50);
		GetSimon().textSize(12);
		GetSimon().text(2, (int)((GetSimon().width / 2 - 20) / 2 - 25), (int)(GetSimon().height *.75 + 5) + 35);
		
		//Upper Right Box (2)
		GetSimon().textSize(24);
		if(GetSimon().mouseX > GetSimon().width / 2 + 20 && GetSimon().mouseX < GetSimon().width - 20)
		{
			if(GetSimon().mouseY > GetSimon().height / 2 + 5 && 
					GetSimon().mouseY < GetSimon().height * (0.75) - 20)
			{
				GetSimon().fill(30,255,255);
			}
			else
			{
				GetSimon().fill(255);
			}
		}
		else
		{
			GetSimon().fill(255);
		}

		GetSimon().rect(
				GetSimon().width / 2 + 20, 
				GetSimon().height / 2 + 5, 
				GetSimon().width - 20 - (GetSimon().width / 2 + 20), 
				(float)(GetSimon().height * (0.75) - 20 - (GetSimon().height / 2 + 5))
				);
		GetSimon().fill(0);
		GetSimon().text(GetAnswers(2).GetAnswer(), (int)((GetSimon().width * .75)), (int)(GetSimon().height / 2 + 5) + 50);
		
		//Lower Right Box (3)
		if(GetSimon().mouseX > GetSimon().width / 2 + 20 && GetSimon().mouseX < GetSimon().width - 20)
		{
			if(GetSimon().mouseY > GetSimon().height * (.75) + 5 &&
					GetSimon().mouseY < GetSimon().height - 20)
			{
				GetSimon().fill(30,255,255);
			}
			else
			{
				GetSimon().fill(255);
			}
		}
		else
		{
			GetSimon().fill(255);
		}
		GetSimon().rect(
				GetSimon().width / 2 + 20, 
				(float)(GetSimon().height * (.75) + 5), 
				GetSimon().width - 20 - (GetSimon().width / 2 + 20), 
				(float)(GetSimon().height - 20 - (GetSimon().height * (.75) + 5))
				);
		GetSimon().fill(0);
		GetSimon().text(GetAnswers(3).GetAnswer(), (int)((GetSimon().width * .75)), (int)(GetSimon().height *.75 + 5) + 50);
	}
}
