package fahooeygame;

import processing.core.PApplet;
import processing.core.PImage;

/***
 * This allows Question 26 (index number 25) to draw a nice sine graph.
 * @author Photon Beam
 */
public class Question25 extends Question
{
	private PImage sineGraph;
	public Question25(PApplet Simon, String[] a, int correctNum)
	{
		super(Simon, null, a, correctNum);
		
		sineGraph = Simon.loadImage("sine.png");
	}
	public void DisplayQuestion()
	{
		/*Obsolete.
		//Translate helps do all the easy work.
		GetSimon().translate(GetSimon().width / 2, (float) (GetSimon().height * .25));
		
		GetSimon().fill(0);
		GetSimon().strokeWeight(2);
		GetSimon().line(0, -100, 0, 100);
		GetSimon().line(-200, 0, 200, 0);

		//Draws a nice not sinual enough sine graph. 
		for(double i = -2 * Math.PI; i <= Math.PI * 2 - 0.02; i += 0.02)
		{
			GetSimon().line((float)(25 * i), (float)(-50 * Math.sin(i)), (float)(25 * i+ 0.02), (float)(-50 * Math.sin(i + 0.02)));
		}
		
		//Retranslated to prevent hassle
		GetSimon().translate(-(GetSimon().width / 2), -((float) (GetSimon().height * .25)));
		*/
		GetSimon().image(sineGraph, (GetSimon().width / 2) - (sineGraph.width / 2), (float) (GetSimon().height * .25) - (sineGraph.height / 2));
	}
}
