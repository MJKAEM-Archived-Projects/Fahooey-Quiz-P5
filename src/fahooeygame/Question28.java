package fahooeygame;

import processing.core.PApplet;

/**
 * This modifies the epic question28 to include the accursed jingle.
 * @author Phalange Digits
 * 
 */
public class Question28 extends Question
{
	private byte jingle1, jingle2, jingle3;
	/**
	 * 
	 * @param Type 'this' or whatever PApplet is passed as.
	 * @param The question. Each position in the array moves it down.  Do not use the backslash n.
	 * @param The array of answers.
	 * @param Value of 0-3, whichever answer is correct. Any other indicates the boxes are all wrong.
	 */
	public Question28(PApplet Simon, String q[], String[] a)
	{
		super(Simon, q, a, 1);
		
	}
	public byte ClickAnswers()
	{
		byte ans = super.ClickAnswers();

		if(ans == 2)
		{
			if(jingle1 != 111)
			{
				if(jingle1 / 1 == 0)
				{
					++jingle1;
					GetAnswers(1).SetCorrect(false);
					GetAnswers(2).SetCorrect(true);
					SetQuestionText(2, GetQuestionText(2) + "sin ");
				}
				else if(jingle1 / 10 == 0)
				{
					jingle1 += 10;
					
					SetQuestionText(2, GetQuestionText(2) + "cos ");
				}
				else if(jingle1 / 100 == 0)
				{
					jingle1 += 100;
					GetAnswers(2).SetCorrect(false);
					GetAnswers(1).SetCorrect(true);
					SetQuestionText(2, GetQuestionText(2) + "cos ");
				}
			}
			else if(jingle2 != 111)
			{
				if(jingle2 / 1 == 0)
				{
					++jingle2;
					GetAnswers(1).SetCorrect(false);
					GetAnswers(0).SetCorrect(true);
					SetQuestionText(2, GetQuestionText(2) + "sin ");
				}
				else if(jingle2 / 10 == 0)
				{
					jingle2 += 10;
					GetAnswers(0).SetCorrect(false);
					GetAnswers(3).SetCorrect(true);
					SetQuestionText(2, "sin\u03B1 cos sin cos");
				}
				else if(jingle2 / 100 == 0) //{"\u03B1", "sine", "cosine", "\u03B2"}
				{
					jingle2 += 100;
					GetAnswers(3).SetCorrect(false);
					GetAnswers(0).SetCorrect(true);
					SetQuestionText(2, "sin\u03B1 cos\u03B2 sin cos");
				}
			}
			else if(jingle3 != 111)
			{
				if(jingle3 / 1 == 0)
				{
					++jingle3;
					GetAnswers(0).SetCorrect(false);
					GetAnswers(3).SetCorrect(true);
					SetQuestionText(2, "sin\u03B1 cos\u03B2 sin\u03B1 cos");
				}
				else if(jingle3 / 10 == 0)
				{
					jingle3 += 10;

					GetAnswers(3).SetCorrect(false);
					GetAnswers(2).SetCorrect(true);
					
					SetQuestionText(2, "sin\u03B1 cos\u03B2 ?? sin\u03B1 cos\u03B2");
					ChangeAnswers(new String[] {
							"Sine stays the same.",
							"Sign changes.",
							"Sign stays the same.",
							"Sine changes."
					});
				}
				else if(jingle3 / 100 == 0)
				{
					jingle1 = 0;
					jingle2 = 0;
					jingle3 = 0;
					ChangeAnswers(new String[] {"\u03B1", "sine", "cosine", "\u03B2"});
					SetQuestionText(2, "");
					
					return 2;
				}
			}
		}
		else if(ans == 1)
		{
			return 1;
		}

		return 0;
	}
}
