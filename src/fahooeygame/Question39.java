package fahooeygame;

import processing.core.PApplet;

public class Question39 extends Question
{
	/**
	 * 
	 * @param Type 'this' or whatever PApplet is passed as.
	 * @param The question. Each position in the array moves it down.  Do not use the backslash n.
	 * @param The array of answers.
	 * @param Value of 0-3, whichever answer is correct. Any other indicates the boxes are all wrong.
	 */
	public Question39(PApplet Simon, String q[], String[] a)
	{
		super(Simon, q, a, -5);
	}
	
	/**
	 * Returns a number indicating whether the buttons are clicked
	 * and whether or not it is correct.
	 * @return 0 for not clicked. 1 for wrong. 2 for correct.
	 */
	public byte ClickAnswers()
	{
		byte temp = super.ClickAnswers();
		
		if(temp == 1)
		{
			return 2;
		}
		return temp;
	}
}
