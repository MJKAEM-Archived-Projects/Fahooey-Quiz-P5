/**READ BEFORE CONTINUING*
 * 
 * This is a satire of the great teacher of Lowell High School.
 * 
 * Due his/her excellent prestige in teaching, this game was made
 * in his/her honor to accurately replicate his/her tests.
 * 
 * Good luck if you can even beat the game with no fails!
 * 
 * Started on 9-19-2013.
 * Finished on 12-16-2013
 */

package fahooeygame;

import javax.swing.ImageIcon;

import processing.core.PApplet;
import processing.core.PFont;
import ddf.minim.*;


public class FahooeyGame extends PApplet
{
	//public static final boolean debugMode = true;
	public static final boolean debugMode = false;

	public static boolean trueTheme;
	public static boolean final10;
	public static PFont font1, font2, bigFont2;

	private AudioPlayer correctSound, wrongSound, final10Sound, startSound, final10Wrong;
	private AudioPlayer backMusic, final10Music;
	private Minim minim;

	private boolean showFR;
	private byte gameState;
	private byte curQuestion;
	private int deathCount;

	private Question[] questions = new Question[50];
	private int[] correct = new int[50];
	private int[] wrong = new int[50];
	private Title title;
	private Instructions instructions;
	private Credits credits;
	private YouWin youWin;

	private MuteSoundButton muteSoundButton;
	private MuteMusicButton muteMusicButton;

	public void setup()
	{
		font1 = loadFont("LucidaSans-24.vlw");
		font2 = loadFont("Chiller-Regular-24.vlw");
		bigFont2 = loadFont("Chiller-Regular-48.vlw");

		textFont(font1);

		instructions = new Instructions(this);
		credits = new Credits(this);
		minim = new Minim(this);
		muteSoundButton = new MuteSoundButton(this);
		muteMusicButton = new MuteMusicButton(this);

		try
		{
			correctSound = minim.loadFile("DING.WAV");
			wrongSound = minim.loadFile("Bike Horn.mp3");
			final10Wrong = minim.loadFile("Sigh.mp3");
			final10Sound = minim.loadFile("nuclear siren.mp3");
			startSound = minim.loadFile("Train_Honk_Horn_Clear.mp3");

			correctSound.mute();
			wrongSound.mute();
			final10Wrong.mute();
			final10Sound.mute();
			startSound.mute();

			//Force loading of sound
			correctSound.play();
			wrongSound.play();
			final10Wrong.play();
			final10Sound.play();
			startSound.play();
		}
		catch(NullPointerException e)
		{
			muteSoundButton.SetMuted((byte) 2);
			correctSound = null;
			wrongSound = null;
			final10Wrong = null;
			final10Sound = null;
			startSound = null;
		}
		try
		{
			if(trueTheme)
			{
				backMusic = minim.loadFile("song000.mp3");
			}
			else
			{
				backMusic = minim.loadFile("song001.mp3");
			}
			final10Music = minim.loadFile("song002.mp3");

			backMusic.mute();
			final10Music.mute();

			//Force loading of sound
			backMusic.play();
			final10Music.play();

		}
		catch(NullPointerException e)
		{
			muteMusicButton.SetMuted((byte) 2);
			backMusic = null;
			final10Music = null;
		}

		frameRate(60);
		size(640, 480);
		//size(800, 600);
		//frame.setResizable(true);

		ImageIcon icon = new ImageIcon(loadBytes("icon.png"));
		frame.setIconImage(icon.getImage());

		frame.setTitle("Fahooey Game");


		title = new Title(this,
				new String[]
						{
				"Fahooey Quiz",
				"by a Frustrated Lunatic",
				"Pre-Calculus 2013"
						},
						new String[] {"Start train", "Credits", "Instructions", "Exit"}
				);

		questions[0] = new Question(this,
				new String[]
						{
				"Which is the next correct",
				"step in getting 0",
				"from sin(180\u00B0)?",
						},
						new String[] {"sin(180\u00B0)", "0", "sin(\u03C0 + 2\u03C0)", "sin(\u03C0)"},
						2);

		questions[1] = new Question(this,
				new String[]
						{
				"What is the most",
				"important part",
				"when graphing?"
						},
						new String[] {"Scaling", "Plotting points", "Using a ruler", "All of the choices"},
						0);

		questions[2] = new Question(this,
				new String[]
						{
				"Correctly address the next",
				"step: x\u00B2 - 25 = 0"
						},
						new String[] {"x = \u00B1\u221A(25)", "x\u00B2 = 25", "(x - 5)(x + 5) = 0", "x = \u00B15"},
						2);

		questions[3] = new Question(this,
				new String[]
						{
				"Print me a picture of"
						},
						new String[] {"Neil Armstrong", "Janet Voss", "Issac Newton", "Alan B. Shepard"},
						1);

		questions[4] = new Question(this,
				new String[]
						{
				"Your circle isn't very"
						},
						new String[] {"Radial", "Scaled", "Round", "Circular"},
						3);

		questions[5] = new Question(this,
				new String[]
						{
				"Your radius isn't very"
						},
						new String[] {"Scaled", "Radial", "Long", "Short"},
						1);

		questions[6] = new Question(this,
				new String[]
						{
				"Your angle isn't very"
						},
						new String[] {"Angular", "Archival", "Anglican", "Archaic"},
						0);

		questions[7] = new Question(this,
				new String[]
						{
				"In English, what is the",
				"next step for 12\u00F72?"
						},
						new String[] {"Divide by 2", "Multiply by 1/2", "Split it in half", "Divvy it up"},
						3);

		questions[8] = new Question(this,
				new String[]
						{
				"When plugging in numbers",
				"into equations, we are",
				"trying to"
						},
						new String[] {"See if it shakes", "Shake it up", "See what shakes", "Be an astronaut"},
						2);

		questions[9] = new Question(this,
				new String[]
						{
				"Not paying attention?",
				"Time for"
						},
						new String[] {"Ritalin spray", "'Suspension' jokes", "Captivation", "Sister Mary Goliath"},
						0);

		questions[10] = new Question(this,
				new String[]
						{
				"Name the first step",
				"when solving",
				"[f(x+h)-f(x)]/h"
						},
						new String[] {"Factor", "Plug numbers into h,\nwhile x = 1", "Divvy it up", "Write the equation\ndown"},
						3);

		questions[11] = new Question(this,
				new String[]
						{
				"I am an expert at",
				"playing the"
						},
						new String[] {"Banjo", "Guitar", "Mandolin", "Fiddle"},
						2);

		questions[12] = new Question(this,
				new String[]
						{
				"\u00BFHablas espa\u00F1ol?"
						},
						new String[] {"Non loquor Spanish.", "No hablos Ingl\u00E9s.", "Je ne parle pas anglais.", "In English, please."},
						0);

		questions[13] = new Question(this,
				new String[]
						{
				"I see you have done",
				"your homework!"
						},
						new String[] {"Upside down cross", "Anti religion", "Devil horns", "Pentagram of evil"},
						3);

		questions[14] = new Question(this,
				new String[]
						{
				"Who loves scaled",
				"graphs the most?"
						},
						new String[] {"Sister Mary Goliath", "Fahooey", "Fahooey's students", "Satan"},
						0);

		questions[15] = new Question(this,
				new String[]
						{
				"Hey!!!"
						},
						new String[] {"Stop wasting time!", "Come back to me!", "Be here now!", "No time to wander!"},
						2);

		questions[16] = new Question(this,
				new String[]
						{
				"I am too lazy to teach,",
				"So go watch"
						},
						new String[] {"Khan Academy", "PatrickJNT", "Khan Acadamy", "PatrickJMT"},
						3);

		questions[17] = new Question17(this,
				new String[]
						{
				"Correctly obtain the cos(0)."
						},
						new String[] {"0", "-0", "-1", "Undefined"}
				);

		questions[18] = new Question18(this,
				new String[]
						{
				"Now, get the secant."
						},
						new String[] {"1/cos(1)", "1", "1/cos(0)", "I forgot the angle."},
						2);

		questions[19] = new Question(this,
				new String[]
						{
				"Doing more than needed",
				"earns you how many",
				"points out of 12?"
						},
						new String[] {"12", "16", "6", "0"},
						3);

		questions[20] = new Question(this,
				new String[]
						{
				"How likely is",
				"partial credit?"
						},
						new String[] {"Go to math lab.", "Almost never.", "20% chance.", "Never."},
						0);

		questions[21] = new Question(this,
				new String[]
						{
				"How likely is",
				"a curve?"
						},
						new String[] {"Go to math lab.", "Almost never.", "20% chance.", "Never."},
						0);

		questions[22] = new Question(this,
				new String[]
						{
				"What happens in",
				"Math Lab?"
						},
						new String[] {"Good grades.", "Conversations.", "Happiness.", "A Great Depression."},
						3);

		questions[23] = new Question(this,
				new String[]
						{
				"\u03C9 stands for"
						},
						new String[] {"Ohms", "Angular speed", "\u03A9", "w"},
						1);

		questions[24] = new Question(this,
				new String[]
						{
				"\u223F is"
						},
						new String[] {"sin(x)", "A sine graph", "Not quite sinual", "Not scaled properly"},
						3);

		questions[25] = new Question25(this,
				new String[] {"Perfect graph", "Sine graph", "A very sinual graph", "Cosine graph"},
				2);

		questions[26] = new Question(this,
				new String[]
						{
				"Test postponed guys!",
				"The test will be on"
						},
						new String[] {"still the same day", "one day after", "the next week", "the next Monday"},
						0);

		questions[27] = new Question(this,
				new String[]
						{
				"Pick the scenario",
				"that makes the most",
				"sense"
						},
						new String[] {"Long test; long day", "Long test; short day", "Short test; long day", "Short test; short day"},
						1);

		questions[28] = new Question28(this,
				new String[]
						{
				"Sum Difference Formula",
				"Jingle For sin(\u03B1 + \u03B2)!",
				""
						},
						new String[] {"\u03B1", "sine", "cosine", "\u03B2"}
				);

		questions[29] = new Question(this,
				new String[]
						{
				"How many posts on",
				"School Loop are needed",
				"to get people's attention?"
						},
						new String[] {"1", "3", "2", "4"},
						1);

		questions[30] = new Question(this,
				new String[]
						{
				"The radius is 20 meters,",
				"1 rev. takes 2 secs.",
				"What is missing?"
						},
						new String[] {"Critical points", "Amplitude", "Dependent variable(s)", "Independent variable(s)"},
						0);

		questions[31] = new Question(this,
				new String[]
						{
				"Choose the best advice",
				"when doing word",
				"problems."
						},
						new String[] {"Draw pictures.", "Domains: [0, \u03C0]", "Check your work.", "Write a summary."},
						3);

		questions[32] = new Question(this,
				new String[]
						{
				"The calculator should"
						},
						new String[] {"check your work.", "not be trusted.", "not be blindly trusted.", "be used as doorstops."},
						2);

		questions[33] = new Question(this,
				new String[]
						{
				"Fall 2013: MRI count is"
						},
						new String[] {"0", "2", "1", "3"},
						1);

		questions[34] = new Question34(this,
				new String[]
						{
				"What configuration",
				"and what law?"
						},
						new String[] {"SAS, Cosine", "SAS, Sine.", "ASS, Sine", "SSA, Cosine"},
						2);

		questions[35] = new Question(this,
				new String[]
						{
				"A boy fails a test",
				"with 35 questions and",
				"doesn't go to math lab.",
				"Why's this fake?"
						},
						new String[] {"He didn't go\nto math lab.", "There are\n35 questions.", "No one fails a test.", "It's not fake; it's true."},
						1);

		questions[36] = new Question(this,
				new String[]
						{
				"PatrickJMT is not only",
				"cool, but he is:"
						},
						new String[] {"Amazing", "Cute", "Tough", "Smart"},
						1);

		questions[37] = new Question37(this,
				new String[]
						{
				"Money goes into a bank",
				"account and you gain",
				"money. What's this called?",
				""
						},
						new String[] {"Amortization", "Anonymity", "Greed", "Astrology"}
				);

		questions[38] = new Question(this,
				new String[]
						{
				"No one's listening",
				"again! You know what",
				"that means!"
						},
						new String[] {"Sister Mary Goliath!", "Mandolin time!", "Suction cup shoes!", "Ritalin spray!"},
						2);

		questions[39] = new Question39(this,
				new String[]
						{
				"People in the hallway",
				"are too loud!"
						},
						new String[] {"Shut the door...", "Shut the door...", "Shut the door...", "Shut the door..."}
				);

		questions[40] = new Question666(this,
				new String[]
						{
				"What is not a",
				"factor in tests?"
						},
						new String[] {"Time", "a\u00B2-b\u00B2", "NASA space centers", "Money"},
						0);


		questions[41] = new Question666(this,
				new String[]
						{
				"What is the next step for induction",
				"after writing down the equation?"
						},
						new String[] {"Basis Statement", "Let n and k be natural numbers", "Deduction Statement", "Induction Statement"},
						1);

		questions[42] = new Question666(this,
				new String[]
						{
				"F...U...N...K...Y...J...O...H...N"
						},
						new String[] {"Tesser to the moon", "The great mathematician!", "Drowning in space", "Saloon, halfway to the moon!"},
						3);

		questions[43] = new Question666(this,
				new String[]
						{
				"Angle of depression..."
						},
						new String[] {"is proportional to your suffering", "does not exist", "is the angle of elevation", "is always related to two cars\nbelow a cliff"},
						2);

		questions[44] = new Question666(this,
				new String[]
						{
				"I play ??? for ???",
						},
						new String[] {"Rock n Roll for elderly", "Jazz for bums", "Rock for hippies", "Clown for students"},
						1);

		questions[45] = new Question45(this,
				new String[]
						{
				"Did you get him...",
						},
						new String[] {"Y...E...S...", "No!", "I...filed...incomplete...", "H...E...L...P..."}
				);

		questions[46] = new Question666(this,
				new String[]
						{
				"I want to give you a star...",
				"I like your homework..."
						},
						new String[] {"0 points", "10 points", "20 points", "25 points"},
						0);

		questions[47] = new Question666(this,
				new String[]
						{
				"Name the greatest mathmetician of",
				"the 21st century."
						},
						new String[] {"Issac Newton", "Janet Voss", "Blaise Pascal", "Grigori Perelman"},
						3);

		questions[48] = new Question666(this,
				new String[]
						{
				"REALITY CHECK:",
				"Are you brainwashed?"
						},
						new String[] {"No! Fahooey is the best!", "No way! I'm feel awesome!", "I..I..don't know...", "SAVE ME"},
						3);

		questions[49] = new Question49(this,
				new String[]
						{
				"I ain't your Antichrist,",
						},
						new String[] {"I'm your Uncle Sam!"}
				);//*/

		/*int[] jack = {0, 0, 0, 0};

		for(int i = 0; i < questions.length && questions[i] != null; ++i)
		{
			int lol = questions[i].GetCorrectAnswer();

			if(lol != -1)
			{
				++jack[lol];
			}
		}
		println(jack);//*/



		//gameState = 1;
		//curQuestion = 39;

		for(int i = questions.length - 1; i >= 0 && questions[i] == null; --i)
		{
			if(i < 40)
			{
				questions[i] = new Question(this,
						new String[] {"Insert question."},
						new String[] {"Answer 0", "Answer 1", "Answer 2", "Answer 3"},
						1);
			}
			else
			{
				questions[i] = new Question666(this,
						new String[] {"H...E...L...P...M...E..."},
						new String[] {"Forgive me Lord, for I have sinned", "666", "13", "444"},
						0);
			}
		}
	}

	public void draw()
	{
		if(final10)
		{
			background(0);
		}
		else
		{
			background(255);
		}
		textAlign(LEFT);
		if(showFR)
		{
			if(final10)
			{
				fill(255);
			}
			else
			{
				fill(0);
			}
			textSize(24);
			text((int)frameRate, width - 50, height / 2);
		}
		textSize(24);
		switch(gameState)
		{
		case 0:
			title.DisplayQuestion();
			title.DisplayAnswers();
			//if(title.GetWaiting() <= 0)
			//{
			muteSoundButton.Display();
			muteMusicButton.Display();
			//}
			break;

		case 1:
			textAlign(LEFT);
			if(final10)
			{
				fill(255);
			}
			else
			{
				fill(0);
			}
			if(curQuestion != 17)
			{
				text("Level: " + (curQuestion + 1), 20, height / 2 - 45);
			}
			else
			{
				text("Level:  8", 20, height / 2 - 45);
			}
			if(curQuestion == 37)
			{
				if(((Question37)questions[37]).GetProcessWait())
				{
					if(muteSoundButton.IsMuted() == 0)
					{
						correctSound.unmute();
						correctSound.rewind();
						correctSound.play();
					}
					correct[curQuestion] += 7;
					++curQuestion;
				}
			}
			text("Fails: " + deathCount, 20, height / 2 - 20);
			questions[curQuestion].DisplayQuestion();
			questions[curQuestion].DisplayAnswers();
			muteSoundButton.Display();
			muteMusicButton.Display();
			break;

		case 2:
			youWin.Show();
			break;

		case 3:
			credits.Show();
			break;

		case 4:
			instructions.Show();
			break;
		}
	}

	public void mouseReleased()
	{
		if(mouseButton == LEFT)
		{
			switch(gameState)
			{
			case 0:
				//if(title.GetWaiting() <= 0)
				//{
				muteSoundButton.Clicked();
				muteMusicButton.Clicked();
				//}
				switch(title.ClickAnswers())
				{
				case 0:
					boolean temp = muteSoundButton.IsMuted() == 0;
					if(temp)
					{
						startSound.unmute();
						startSound.rewind();
						startSound.play();
					}
					if(temp)
					{
						backMusic.unmute();
						backMusic.rewind();
						backMusic.play();
					}
					curQuestion = 0;
					deathCount = 0;
					for(int i = wrong.length - 1; i >= 0; --i)
					{
						wrong[i] = 0;
					}
					for(int i = correct.length - 1; i >= 0; --i)
					{
						correct[i] = 0;
					}
					gameState = 1;
					break;
				case 1:
					gameState = 3;
					break;
				case 2:
					gameState = 4;
					break;
				case 3:
					exit();
					break;
				}
				break;

			case 1:
				muteSoundButton.Clicked();
				if(muteMusicButton.Clicked())
				{
					byte temp = muteMusicButton.IsMuted();
					if(temp == 1)
					{
						backMusic.mute();
						final10Music.mute();
					}
					else if(temp == 0)
					{
						if(final10)
						{
							final10Music.unmute();
							final10Music.rewind();
							final10Music.play();
						}
						else
						{
							backMusic.unmute();
							backMusic.rewind();
							backMusic.play();
						}
					}
				}

				boolean temp = muteSoundButton.IsMuted() == 0;

				if(!temp)
				{
					final10Sound.mute();
				}

				switch(questions[curQuestion].ClickAnswers())
				{
				case 1:
					if(temp)
					{
						if(final10)
						{
							final10Wrong.unmute();
							final10Wrong.rewind();
							final10Wrong.play();
						}
						else
						{
							wrongSound.unmute();
							wrongSound.rewind();
							wrongSound.play();
						}
					}
					wrong[curQuestion] += 3;
					++deathCount;
					break;
				case 2:
					if(temp)
					{
						if(curQuestion != 39)
						{
							correctSound.unmute();
							correctSound.rewind();
							correctSound.play();
						}
						else
						{
							final10Sound.unmute();
							final10Sound.rewind();
							final10Sound.play();
						}
					}
					if(curQuestion == 39)
					{
						final10 = true;
						textFont(font2);

						if(muteMusicButton.IsMuted() == 0)
						{
							backMusic.mute();

							final10Music.unmute();
							final10Music.rewind();
							final10Music.play();
						}
					}
					correct[curQuestion] += 7;
					++curQuestion;
					if(curQuestion >= 50)
					{
						final10 = false;
						textFont(font1);
						youWin = new YouWin(this, correct, wrong, deathCount);
						backMusic.pause();
						final10Music.pause();
						gameState = 2;
					}
					break;

				case (byte)128:
					if(temp)
					{
						wrongSound.unmute();
						wrongSound.rewind();
						wrongSound.play();
					}
				wrong[curQuestion] += 3;
				++deathCount;
				--curQuestion;
				break;
				}
				break;

			case 2:
				if(youWin.ClickExit())
				{
					gameState = 0;
				}
				break;

			case 3:
				if(credits.ClickExit())
				{
					gameState = 0;
				}
				break;
			case 4:
				if(instructions.ClickExit())
				{
					gameState = 0;
				}
				break;
			}
		}
	}
	public void keyReleased()
	{
		if(key == '`')
		{
			showFR = !showFR;
		}
		if(curQuestion == 37)
		{
			((Question37)questions[37]).AdvanceQuestion(key);
		}
	}

	public void keyPressed()
	{
		if(key == ESC)
		{
			key = 0;
		}
	}
	public static void main(String argv[])
	{
		if(argv.length > 0 && argv[0].equalsIgnoreCase("sgt.pepper"))
		{
			trueTheme = true;
		}

		PApplet.main(new String[] { fahooeygame.FahooeyGame.class.getName() });
	}
}
