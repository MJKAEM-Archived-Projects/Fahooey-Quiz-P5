package fahooeygame;

import processing.core.PApplet;

public class Question18 extends Question
{
	/**
	 * 
	 * @param Type 'this' or whatever PApplet is passed as.
	 * @param The question. Each position in the array moves it down.  Do not use the backslash n.
	 * @param The array of answers.
	 * @param Value of 0-3, whichever answer is correct. Any other indicates the boxes are all wrong.
	 */
	public Question18(PApplet Simon, String q[], String[] a, int correctNum)
	{
		super(Simon, q, a, correctNum);
	}
	
	/**
	 * Returns a number indicating whether the buttons are clicked
	 * and whether or not it is correct.
	 * @return 0 for not clicked. 1 for wrong. 2 for correct.
	 */
	public byte ClickAnswers()
	{
		if(GetSimon().mouseX > 20 && GetSimon().mouseX < GetSimon().width / 2 - 20)
		{
			if(GetSimon().mouseY > GetSimon().height / 2 + 5 && 
					GetSimon().mouseY < GetSimon().height * (0.75) - 20)
			{
				if(GetAnswers(0).IsCorrect())
				{
					return 2;
				}
				else
				{
					return 1;
				}
			}
			else if(GetSimon().mouseY > GetSimon().height * (.75) + 5 &&
					GetSimon().mouseY < GetSimon().height - 20)
			{
				if(GetAnswers(1).IsCorrect())
				{
					return 2;
				}
				else
				{
					return 1;
				}
			}
		}
		else if(GetSimon().mouseX > GetSimon().width / 2 + 20 && GetSimon().mouseX < GetSimon().width - 20)
		{
			if(GetSimon().mouseY > GetSimon().height / 2 + 5 && 
					GetSimon().mouseY < GetSimon().height * (0.75) - 20)
			{
				if(GetAnswers(2).IsCorrect())
				{
					return 2;
				}
				else
				{
					return 1;
				}
			}
			else if(GetSimon().mouseY > GetSimon().height * (.75) + 5 &&
					GetSimon().mouseY < GetSimon().height - 20)
			{
				if(GetAnswers(3).IsCorrect())
				{
					return 2;
				}
				else
				{
					return (byte)128;
				}
			}
		}
		return 0;
	}
}
