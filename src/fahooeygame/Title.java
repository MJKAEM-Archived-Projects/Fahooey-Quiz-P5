package fahooeygame;

import processing.core.PApplet;

/**
 * Questions are self explanatory.
 * As for the waiting variable,
 * it is obsolete now.
 * @author Yim Yam
 */
public class Title extends Question
{
	//public int waiting;

	/**
	 * Initializes a new title screen. It's just a question in disguised.
	 * @param Type 'this' or whatever PApplet is passed as.
	 * @param The game's title screen text.
	 * @param The game's title screen choices.
	 */
	public Title(PApplet Simon, String q[], String[] a)
	{
		super(Simon, q, a, -1);
		//waiting = 60 * 4;
	}

	/**
	 * If the mouse is within any of the boxes, it will return the
	 * ID of the boxes. Otherwise, it will return -1.
	 * @return -1 for not clicked. Every other number is the ID.
	 */
	public byte ClickAnswers()
	{
		//if(waiting <= 0)
		//{
			if(GetSimon().mouseX > 20 && GetSimon().mouseX < GetSimon().width / 2 - 20)
			{
				if(GetSimon().mouseY > GetSimon().height / 2 + 5 && 
						GetSimon().mouseY < GetSimon().height * (0.75) - 20)
				{
					return 0;
				}
				else if(GetSimon().mouseY > GetSimon().height * (.75) + 5 &&
						GetSimon().mouseY < GetSimon().height - 20)
				{
					return 1;
				}
			}
			else if(GetSimon().mouseX > GetSimon().width / 2 + 20 && GetSimon().mouseX < GetSimon().width - 20)
			{
				if(GetSimon().mouseY > GetSimon().height / 2 + 5 && 
						GetSimon().mouseY < GetSimon().height * (0.75) - 20)
				{
					return 2;
				}
				else if(GetSimon().mouseY > GetSimon().height * (.75) + 5 &&
						GetSimon().mouseY < GetSimon().height - 20)
				{
					return 3;
				}
			}
		//}
		return -1;
	}
	
	/**
	 * Displays the game's name on the screen.
	 */
	public void DisplayQuestion()
	{
		//if(waiting <= 0)
		//{
			GetSimon().fill(0);
			GetSimon().textSize(48);
			GetSimon().textAlign(GetSimon().CENTER);

			GetSimon().text(GetQuestionText(0), GetSimon().width / 2, 50);

			for(int i = GetQuestionText().length - 1; i >= 1; i--)
			{
				GetSimon().textSize(28);
				GetSimon().text(GetQuestionText(i), GetSimon().width / 2, 50 + (34 * i));
			}
		//}
		/*else
		{
			GetSimon().textAlign(GetSimon().CENTER);
			GetSimon().textSize(30);
			GetSimon().fill(0);
			GetSimon().text("Please wait. Game is", GetSimon().width / 2, GetSimon().height / 2 - 50);
			GetSimon().text("Warming up to prevent", GetSimon().width / 2, GetSimon().height / 2 + 35 - 50);
			GetSimon().text("lag related issues.", GetSimon().width / 2, GetSimon().height / 2 + 70 - 50);
			GetSimon().text((int)(waiting / 60), GetSimon().width / 2, GetSimon().height / 2 + 105 - 50);

			waiting--;
		}*/
	}
	/*public void DisplayAnswers()
	{
		if(waiting <= 0)
		{
			super.DisplayAnswers();
		}
	}
	public int GetWaiting() { return waiting; }
	public void SetWaiting(int n) { waiting = n; }
	*/
}
