package fahooeygame;

import processing.core.PApplet;

public class MuteSoundButton
{
	private PApplet Simon;
	private byte muted;

	public MuteSoundButton(PApplet Simon)
	{
		this.Simon = Simon;
	}
	/**
	 * Displays the mute button for sound.
	 */
	public void Display()
	{
		//Draw box
		if(Simon.mouseX > Simon.width - 60 && Simon.mouseX < Simon.width - 60 + 35)
		{
			if(Simon.mouseY > Simon.height / 2 - 60 && Simon.mouseY < Simon.height / 2 - 60 + 35)
			{
				if(FahooeyGame.final10)
				{
					Simon.fill(100);
				}
				else
				{
					Simon.fill(30, 255, 30);
				}
			}
			else
			{
				Simon.fill(255);
			}
		}
		else
		{
			Simon.fill(255);
		}

		Simon.strokeWeight(1);
		Simon.rect(Simon.width - 60, Simon.height / 2 - 60, 35, 35);

		//Draw the S
		Simon.textSize(24);
		Simon.textAlign(Simon.CENTER);
		Simon.fill(0);
		Simon.text('S', (float)(Simon.width - 60 + 17.5/*47.5*/), Simon.height / 2 - 35);

		if(muted == 1 || muted == 2)
		{
			Simon.strokeWeight(2);
			Simon.line(Simon.width - 60, Simon.height / 2 - 60, Simon.width - 60 + 35, Simon.height / 2 - 60 + 35);
		}
	}
	/**
	 * If box is clicked, set to false.
	 */
	public boolean Clicked()
	{
		if(Simon.mouseX > Simon.width - 60 && Simon.mouseX < Simon.width - 60 + 35)
		{
			if(Simon.mouseY > Simon.height / 2 - 60 && Simon.mouseY < Simon.height / 2 - 60 + 35)
			{
				switch(muted)
				{
				case 0:
					muted = 1;
					break;
				case 1:
					muted = 0;
					break;
				}
				return true;
			}
		}
		return false;
	}
	/**
	 * Returns whether the sound is muted or not.
	 * @return True or false
	 */
	public byte IsMuted() { return muted; }
	public void SetMuted(byte num) { muted = num; }
}
