package fahooeygame;

import processing.core.PApplet;

/**
 * Actually, it isn't really instructions.
 * @author martino
 *
 */
public class Instructions
{
	private PApplet Simon;
	private String[] text;

	public Instructions(PApplet Simon)
	{
		this.Simon = Simon;
		text = new String[] 
				{
				"Fahooey Quiz. You know the math, and all the stuff.",
				"Except, you are still wrong because you did not",
				"write the answer the way your teacher wanted it.",
				"We know how you feel. We made this quiz so that",
				"when you take it, you will be 'guaranteed' to pass",
				"any test if format means everything.",
				"",
				"So go forth, and solve the questions, my lord.",
				};
	}
	public void Show()
	{
		Simon.fill(0);
		Simon.textSize(24);
		Simon.textAlign(Simon.CENTER);

		//Display all the text
		for(int i = text.length - 1; i >= 0; i--)
		{
			Simon.text(text[i], Simon.width / 2, 50 + (30 * i));
		}

		//If mouse over box, highlight the box
		if(Simon.mouseX > 20 && Simon.mouseX < 20 + 300)
		{
			if(Simon.mouseY > Simon.height - 20 - 50 && Simon.mouseY < Simon.height - 20)
			{
				Simon.fill(30, 255, 255);
			}
			else
			{
				Simon.fill(255);
			}
		}
		else
		{
			Simon.fill(255);
		}
		Simon.rect(20, Simon.height - 20, 300, -50);
		Simon.fill(0);
		Simon.text("Back to main menu", 160, Simon.height - 40);
	}
	/**
	 * Tells game whether or not to goes back to main menu.
	 * @param Type 'this' or whatever PApplet is passed as.
	 * @return True or false.
	 */
	public boolean ClickExit()
	{
		if(Simon.mouseX > 20 && Simon.mouseX < 20 + 300)
		{
			if(Simon.mouseY > Simon.height - 20 - 50 && Simon.mouseY < Simon.height - 20)
			{
				return true;
			}
		}
		return false;
	}
}
