package fahooeygame;

import processing.core.PApplet;
import processing.core.PImage;

/**
 * It is a triangle, you ASS (Angle Side Side).
 * @author Super Duper Super Man
 *
 */
public class Question34 extends Question
{
	private PImage Triangle;
	public Question34(PApplet Simon, String q[], String[] a, int correctNum)
	{
		super(Simon, q, a, correctNum);
		
		Triangle = Simon.loadImage("Triangle.png");
	}
	public void DisplayQuestion()
	{
		super.DisplayQuestion();
		GetSimon().image(Triangle, (GetSimon().width / 2) - (Triangle.width / 2), (float) (GetSimon().height * .25) - (Triangle.height / 2));
	}
}
