package fahooeygame;

import processing.core.PApplet;

/**
 * Fun is infinite with Fahooey. -Majin
 * @author nataS
 *
 */
public class Question666 extends Question
{
	/**
	 * 
	 * @param Type 'this' or whatever PApplet is passed as.
	 * @param The question. Each position in the array moves it down.  Do not use the backslash n.
	 * @param The array of answers.
	 * @param Value of 0-3, whichever answer is correct. Any other indicates the boxes are all wrong.
	 */
	public Question666(PApplet Simon, String q[], String[] a, int correctNum)
	{
		super(Simon, q, a, correctNum);
	}

	/**
	 * Displays the question at the top of the screen.
	 */
	public void DisplayQuestion()
	{
		GetSimon().fill(255);
		GetSimon().textFont(FahooeyGame.bigFont2);
		GetSimon().textSize(48);
		GetSimon().textAlign(GetSimon().CENTER);

		for(int i = GetQuestionText().length - 1; i >= 0; --i)
		{
			GetSimon().text(GetQuestionText(i), GetSimon().width / 2, 50 + (50 * i));
		}
	}
	public void DisplayAnswers()
	{
		GetSimon().textFont(FahooeyGame.font2);
		GetSimon().textSize(24);
		GetSimon().textAlign(GetSimon().CENTER);
		GetSimon().strokeWeight(1);

		//Upper Left Box (0)
		FillerBox(
				(GetSimon().mouseX > 20 && GetSimon().mouseX < GetSimon().width / 2 - 20),
				(GetSimon().mouseY > GetSimon().height / 2 + 5 && GetSimon().mouseY < GetSimon().height * (0.75) - 20)
				);

		GetSimon().rect(
				20, 
				GetSimon().height / 2 + 5, 
				GetSimon().width / 2 - 20 - 20, 
				(float)(GetSimon().height * (0.75) - 20 - (GetSimon().height / 2 + 5))
				);


		GetSimon().fill(255);
		GetSimon().text(GetAnswers(0).GetAnswer(), (int)((GetSimon().width / 2 - 20) / 2), (int)(GetSimon().height / 2 + 5) + 50);

		//Lower Left Box (1)
		FillerBox(
				(GetSimon().mouseX > 20 && GetSimon().mouseX < GetSimon().width / 2 - 20),
				(GetSimon().mouseY > GetSimon().height * (.75) + 5 && GetSimon().mouseY < GetSimon().height - 20)
				);

		GetSimon().rect(
				20, 
				(float)(GetSimon().height * (.75) + 5), 
				GetSimon().width / 2 - 20 - 20, 
				(float)(GetSimon().height - 20 - (GetSimon().height * (.75) + 5))
				);

		GetSimon().fill(255);
		GetSimon().text(GetAnswers(1).GetAnswer(), (int)((GetSimon().width / 2 - 20) / 2), (int)(GetSimon().height *.75 + 5) + 50);

		//Upper Right Box (2)
		FillerBox(
				(GetSimon().mouseX > GetSimon().width / 2 + 20 && GetSimon().mouseX < GetSimon().width - 20),
				(GetSimon().mouseY > GetSimon().height / 2 + 5 && GetSimon().mouseY < GetSimon().height * (0.75) - 20)
				);


		GetSimon().rect(
				GetSimon().width / 2 + 20, 
				GetSimon().height / 2 + 5, 
				GetSimon().width - 20 - (GetSimon().width / 2 + 20), 
				(float)(GetSimon().height * (0.75) - 20 - (GetSimon().height / 2 + 5))
				);

		GetSimon().fill(255);
		GetSimon().text(GetAnswers(2).GetAnswer(), (int)((GetSimon().width * .75)), (int)(GetSimon().height / 2 + 5) + 50);

		//Lower Right Box (3)
		FillerBox(
				(GetSimon().mouseX > GetSimon().width / 2 + 20 && GetSimon().mouseX < GetSimon().width - 20),
				(GetSimon().mouseY > GetSimon().height * (.75) + 5 && GetSimon().mouseY < GetSimon().height - 20)
				);

		GetSimon().rect(
				GetSimon().width / 2 + 20, 
				(float)(GetSimon().height * (.75) + 5), 
				GetSimon().width - 20 - (GetSimon().width / 2 + 20), 
				(float)(GetSimon().height - 20 - (GetSimon().height * (.75) + 5))
				);


		GetSimon().fill(255);
		GetSimon().text(GetAnswers(3).GetAnswer(), (int)((GetSimon().width * .75)), (int)(GetSimon().height *.75 + 5) + 50);
	}

	protected void FillerBox(boolean a, boolean b)
	{
		if(a)
		{
			if(b)
			{
				GetSimon().fill(175,0,0);
			}
			else
			{
				GetSimon().fill(45);
			}
		}
		else
		{

			GetSimon().fill(45);
		}
	}
}
