package fahooeygame;

import processing.core.PApplet;

/**
 * Money, money, money!  DOSH GRAB IT WHILE IT'S HOT!
 * LOADS OF MONEY! FSADAFASGA
 * @author Posh Dosh
 *
 */
public class Question37 extends Question
{
	private int wait;
	/**
	 * 
	 * @param Type 'this' or whatever PApplet is passed as.
	 * @param The question. Each position in the array moves it down.  Do not use the backslash n.
	 * @param The array of answers.
	 * @param Value of 0-3, whichever answer is correct. Any other indicates the boxes are all wrong.
	 */
	public Question37(PApplet Simon, String q[], String[] a)
	{
		super(Simon, q, a, 4);
		
		wait = 1 * 60;
	}
	
	public void AdvanceQuestion(char key)
	{
		if(ProcessAnswer(key))
		{
			if(GetQuestionText(3).equals(""))
			{
				SetQuestionText(3, "A");
			}
			else if(GetQuestionText(3).equals("A"))
			{
				SetQuestionText(3, "An");
			}
			else if(GetQuestionText(3).equals("An"))
			{
				SetQuestionText(3, "Ann");
			}
			else if(GetQuestionText(3).equals("Ann"))
			{
				SetQuestionText(3, "Annu");
			}
			else if(GetQuestionText(3).equals("Annu"))
			{
				SetQuestionText(3, "Annui");
			}
			else if(GetQuestionText(3).equals("Annui"))
			{
				SetQuestionText(3, "Annuit");
			}
			else if(GetQuestionText(3).equals("Annuit"))
			{
				SetQuestionText(3, "Annuity");
			}
		}
	}
	public boolean ProcessAnswer(char key)
	{
		if(GetQuestionText(3).equals(""))
		{
			return key == 'a';
		}
		else if(GetQuestionText(3).equals("A"))
		{
			return key == 'n';
		}
		else if(GetQuestionText(3).equals("An"))
		{
			return key == 'n';
		}
		else if(GetQuestionText(3).equals("Ann"))
		{
			return key == 'u';
		}
		else if(GetQuestionText(3).equals("Annu"))
		{
			return key == 'i';
		}
		else if(GetQuestionText(3).equals("Annui"))
		{
			return key == 't';
		}
		else if(GetQuestionText(3).equals("Annuit"))
		{
			return key == 'y';
		}
		else
		{
			return false;
		}
	}
	public void DisplayAnswers()
	{
		super.DisplayAnswers();
		
		if(GetQuestionText(3).equals("Annuity"))
		{
			--wait;
		}
	}
	
	public boolean GetProcessWait()
	{
		if(wait == 0)
		{
			wait = 1 * 60;
			SetQuestionText(3, "");
			return true;
		}
		return false;
	}
}
