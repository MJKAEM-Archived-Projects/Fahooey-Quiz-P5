package fahooeygame;

import processing.core.PApplet;

/**
 * Actually, you don't win; you survive.
 * @author Billy Rumblespeare
 *
 */
public class YouWin
{
	private PApplet Simon;
	private String[] text;
	private boolean legit;

	/**
	 * You win, unless you cheated.
	 * The anti cheat won't affect you if you play fair.
	 * By the way, the first anti cheat measure was making
	 * the curQuestion a byte, meaning you would have to
	 * manually change CE settings to scan byte.
	 * @param Type 'this' or whatever PApplet is passed as.
	 * @param Type correct, which is an array of correct answers.
	 * @param Type wrong, which is an array of wrong answers.
	 * @param Type deathCount, the normal count of how many deaths.
	 */
	public YouWin(PApplet Simon, int[] r, int[] w, int wr)
	{
		this.Simon = Simon;

		//This boolean will determine the outcome of the game.
		legit = true;

		//If not debugging, the anti cheat will kick in.
		if(!FahooeyGame.debugMode)
		{
			//This int will calculate your real numbers wrong.
			int wrong = 0;

			//Calculation to see if you really clicked your way to the end.
			for(int i = r.length - 1; i >= 0; --i)
			{
				if(r[i] % 7 != 0)
				{
					legit = false;
					break;
				}
			}
			if(legit)
			{
				//Calculation to see if you really clicked how many you really got wrong.
				for(int i = w.length - 1; i >= 0; --i)
				{
					wrong += (w[i] / 3);
				}
				legit = (wrong == wr);
			}
		}

		if(legit)
		{
			text = new String[] 
					{
					"You have barely survived with:",
					wr + " fails.",
					"",
					"Well, I don't know what to say. You better go to",
					"Math Lab. That way, you can make up for your lack",
					"of understanding. Everything you've learned in the",
					"past is all wrong. Next time, take better notes in",
					"class.",
					"",
					"You have (unfairly) received an:",
					"F",
					};

			//Give out ranks.
			switch(wr)
			{
			case 0:
				text = new String[] 
						{
						"You have won with:",
						"No fails!",
						"",
						"I congratulate you! You are most certainly qualified",
						"to be an astronaut in NASA! Go forth and fulfill",
						"your destiny! If you fail, you can always teach at", 
						"Stanford, then move on as a high school math",
						"teacher...",
						"",
						"You passed with an:",
						"A",
						};
				break;
			case 1:
				text = new String[] 
						{
						"You have won with:",
						"One fail.",
						"",
						"I congratulate you! You are most certainly qualified",
						"to be an astronaut in NASA! Go forth and fulfill",
						"your destiny! If you fail, you can always teach at", 
						"Stanford, then move on as a high school math",
						"teacher...",
						"",
						"You passed with an:",
						"A",
						};
				break;
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
				text = new String[] 
						{
						"You have won with:",
						wr + " fails.",
						"",
						"Darn, it seems you did quite well; there's no need to",
						"go to math lab... however, based on your score, you",
						"are very likely going to be a high school math",
						"teacher. No need to fear; this quiz can be the base",
						"for your teachings!",
						"",
						"You passed with a:",
						"B",
						};
				break;
			case 7:
			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
				text = new String[] 
						{
						"You have survived with:",
						wr + " fails.",
						"",
						"This score isn't looking quite good. It is average,",
						"but I recommend going to Math Lab to fix that grade.",
						"Clearly, you haven't been taking very good notes and", 
						"you have messy homework, so the blame is on you.",
						"",
						"You passed with a:",
						"C",
						};
				break;
			case 15:
			case 16:
			case 17:
			case 18:
			case 19:
			case 20:
				text = new String[] 
						{
						"You have barely survived with:",
						wr + " fails.",
						"",
						"This grade is your fault. You obviously didn't do",
						"your homework 'correctly', and you haven't shown",
						"that you have learned anything. If I were you, I'd", 
						"go to Math Lab.",
						"",
						"You got a:",
						"D",
						};
				break;
			}
		}
		else
		{
			text = new String[] 
					{
					"You thought you could easily cheat?",
					"Well tough luck! You and your Cheat",
					"Engine were no match for this quiz!",
					"Redo the quiz, script kiddie!",
					"",
					"You failed, now go home with an F!"
					};
		}
	}
	public void Show()
	{
		Simon.fill(0);
		Simon.textSize(24);
		Simon.textAlign(Simon.CENTER);

		for(int i = text.length - 1; i >= 0; --i)
		{
			Simon.text(text[i], Simon.width / 2, 50 + (30 * i));
		}

		if(Simon.mouseX > 20 && Simon.mouseX < 20 + 300)
		{
			if(Simon.mouseY > Simon.height - 20 - 50 && Simon.mouseY < Simon.height - 20)
			{
				Simon.fill(30, 255, 255);
			}
			else
			{
				Simon.fill(255);
			}
		}
		else
		{
			Simon.fill(255);
		}
		Simon.rect(20, Simon.height - 20, 300, -50);
		Simon.fill(0);
		Simon.text("Back to main menu", 160, Simon.height - 40);
	}
	/**
	 * Tells game whether or not to goes back to main menu.
	 * @return True or false.
	 */
	public boolean ClickExit()
	{
		if(Simon.mouseX > 20 && Simon.mouseX < 20 + 300)
		{
			if(Simon.mouseY > Simon.height - 20 - 50 && Simon.mouseY < Simon.height - 20)
			{
				return true;
			}
		}
		return false;
	}
}
