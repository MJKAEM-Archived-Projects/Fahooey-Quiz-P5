package fahooeygame;

import processing.core.PApplet;

public class Question17 extends Question
{
	public Question17(PApplet Simon, String q[], String[] a)
	{
		super(Simon, q, a, 4);
	}
	public void DisplayAnswers()
	{
		super.DisplayAnswers();

		if(GetSimon().mouseX > 90 && GetSimon().mouseX < 90 + 10)
		{
			if(GetSimon().mouseY > GetSimon().height / 2 - 65 && GetSimon().mouseY < GetSimon().height / 2 - 65 + 20)
			{
				GetSimon().fill(30, 255, 255);
			}
		}
		else
		{
			GetSimon().fill(0);
		}
		GetSimon().text(1, 95, GetSimon().height / 2 - 45);
	}
	public byte ClickAnswers()
	{
		if(GetSimon().mouseX > 90 && GetSimon().mouseX < 90 + 10)
		{
			if(GetSimon().mouseY > GetSimon().height / 2 - 65 && GetSimon().mouseY < GetSimon().height / 2 - 65 + 20)
			{
				return 2;
			}
		}
		return super.ClickAnswers();
	}
}
