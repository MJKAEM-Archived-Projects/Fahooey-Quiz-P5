package fahooeygame;

public class Answers
{
	private boolean isCorrect;
	private String answerText;
	
	/***
	 * An answer to a question.
	 * @param Determines if it is the correct answer.
	 * @param The text of the answer.
	 */
	public Answers(boolean correct, String text)
	{
		isCorrect = correct;
		answerText = text;
	}
	/**
	 * Returns whether the answer is correct or not.
	 * @return True or false.
	 */
	public boolean IsCorrect()
	{
		return isCorrect;
	}
	public void SetCorrect(boolean bool)
	{
		isCorrect = bool;
	}
	/**
	 * Returns the String of the answer.
	 * @return String
	 */
	public String GetAnswer()
	{
		return answerText;
	}
	public void SetAnswer(String str)
	{
		answerText = str;
	}
}
