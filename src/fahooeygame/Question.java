package fahooeygame;

import processing.core.PApplet;

public class Question
{
	private PApplet Simon;
	private String[] questionText;
	private Answers[] answers;

	/**
	 * 
	 * @param Type 'this' or whatever PApplet is passed as.
	 * @param The question. Each position in the array moves it down.  Do not use the backslash n.
	 * @param The array of answers.
	 * @param Value of 0-3, whichever answer is correct. Any other indicates the boxes are all wrong.
	 */
	public Question(PApplet Simon, String q[], String[] a, int correctNum)
	{
		this.Simon = Simon;
		questionText = q;
		answers = new Answers[a.length];
		for(int i = a.length - 1; i >= 0; --i)
		{
			if(i != correctNum)
			{
				answers[i] = new Answers(false, a[i]);
			}
			else
			{
				answers[i] = new Answers(true, a[i]);
			}
		}
	}
	/**
	 * Returns a number indicating whether the buttons are clicked
	 * and whether or not it is correct.
	 * @return 0 for not clicked. 1 for wrong. 2 for correct.
	 */
	public byte ClickAnswers()
	{
		if(Simon.mouseX > 20 && Simon.mouseX < Simon.width / 2 - 20)
		{
			if(Simon.mouseY > Simon.height / 2 + 5 && 
					Simon.mouseY < Simon.height * (0.75) - 20)
			{
				if(answers[0].IsCorrect())
				{
					return 2;
				}
				else
				{
					return 1;
				}
			}
			else if(Simon.mouseY > Simon.height * (.75) + 5 &&
					Simon.mouseY < Simon.height - 20)
			{
				if(answers[1].IsCorrect())
				{
					return 2;
				}
				else
				{
					return 1;
				}
			}
		}
		else if(Simon.mouseX > Simon.width / 2 + 20 && Simon.mouseX < Simon.width - 20)
		{
			if(Simon.mouseY > Simon.height / 2 + 5 && 
					Simon.mouseY < Simon.height * (0.75) - 20)
			{
				if(answers[2].IsCorrect())
				{
					return 2;
				}
				else
				{
					return 1;
				}
			}
			else if(Simon.mouseY > Simon.height * (.75) + 5 &&
					Simon.mouseY < Simon.height - 20)
			{
				if(answers[3].IsCorrect())
				{
					return 2;
				}
				else
				{
					return 1;
				}
			}
		}
		return 0;
	}
	/**
	 * Displays the question at the top of the screen.
	 */
	public void DisplayQuestion()
	{
		Simon.fill(0);
		Simon.textSize(48);
		Simon.textAlign(Simon.CENTER);

		for(int i = questionText.length - 1; i >= 0; --i)
		{
			Simon.text(questionText[i], Simon.width / 2, 50 + (50 * i));
		}
	}
	public void DisplayAnswers()
	{
		Simon.textSize(24);
		Simon.textAlign(Simon.CENTER);
		Simon.strokeWeight(1);

		//Upper Left Box (0)
		FillerBox(
				(Simon.mouseX > 20 && Simon.mouseX < Simon.width / 2 - 20),
				(Simon.mouseY > Simon.height / 2 + 5 && Simon.mouseY < Simon.height * (0.75) - 20)
				);

		Simon.rect(
				20, 
				Simon.height / 2 + 5, 
				Simon.width / 2 - 20 - 20, 
				(float)(Simon.height * (0.75) - 20 - (Simon.height / 2 + 5))
				);

		Simon.fill(0);
		Simon.text(answers[0].GetAnswer(), (int)((Simon.width / 2 - 20) / 2), (int)(Simon.height / 2 + 5) + 50);

		//Lower Left Box (1)
		FillerBox(
				(Simon.mouseX > 20 && Simon.mouseX < Simon.width / 2 - 20),
				(Simon.mouseY > Simon.height * (.75) + 5 && Simon.mouseY < Simon.height - 20)
				);

		Simon.rect(
				20, 
				(float)(Simon.height * (.75) + 5), 
				Simon.width / 2 - 20 - 20, 
				(float)(Simon.height - 20 - (Simon.height * (.75) + 5))
				);

		Simon.fill(0);
		Simon.text(answers[1].GetAnswer(), (int)((Simon.width / 2 - 20) / 2), (int)(Simon.height *.75 + 5) + 50);

		//Upper Right Box (2)
		FillerBox(
				(Simon.mouseX > Simon.width / 2 + 20 && Simon.mouseX < Simon.width - 20),
				(Simon.mouseY > Simon.height / 2 + 5 && Simon.mouseY < Simon.height * (0.75) - 20)
				);


		Simon.rect(
				Simon.width / 2 + 20, 
				Simon.height / 2 + 5, 
				Simon.width - 20 - (Simon.width / 2 + 20), 
				(float)(Simon.height * (0.75) - 20 - (Simon.height / 2 + 5))
				);

		Simon.fill(0);
		Simon.text(answers[2].GetAnswer(), (int)((Simon.width * .75)), (int)(Simon.height / 2 + 5) + 50);

		//Lower Right Box (3)
		FillerBox(
				(Simon.mouseX > Simon.width / 2 + 20 && Simon.mouseX < Simon.width - 20),
				(Simon.mouseY > Simon.height * (.75) + 5 && Simon.mouseY < Simon.height - 20)
				);

		Simon.rect(
				Simon.width / 2 + 20, 
				(float)(Simon.height * (.75) + 5), 
				Simon.width - 20 - (Simon.width / 2 + 20), 
				(float)(Simon.height - 20 - (Simon.height * (.75) + 5))
				);


		Simon.fill(0);
		Simon.text(answers[3].GetAnswer(), (int)((Simon.width * .75)), (int)(Simon.height *.75 + 5) + 50);
	}

	protected void FillerBox(boolean a, boolean b)
	{
		if(a)
		{
			if(b)
			{
				Simon.fill(30,255,255);
			}
			else
			{
				Simon.fill(255);
			}
		}
		else
		{
			Simon.fill(255);
		}
	}

	public int GetCorrectAnswer() 
	{ 
		for(int i = answers.length - 1; i >= 0; --i)
		{
			if(answers[i].IsCorrect())
			{
				return i;
			}
		}
		return -1;
	}

	public void ChangeAnswers(String[] str) { for(int i = answers.length - 1; i >= 0; i--){ answers[i].SetAnswer(str[i]); } }

	public Answers GetAnswers(int index) { return answers[index]; }

	public String GetQuestionText(int index) { return questionText[index]; }
	public void SetQuestionText(int index, String str) { questionText[index] = str; }
	public String[] GetQuestionText() { return questionText; }

	protected PApplet GetSimon() { return Simon; }
}
