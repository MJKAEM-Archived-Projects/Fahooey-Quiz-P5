package fahooeygame;

import processing.core.PApplet;

public class Credits
{
	private PApplet Simon;
	private String[] text;

	/**
	 * Creates the credits for the game.
	 * @param Type 'this' or whatever PApplet is passed as.
	 */
	public Credits(PApplet Simon)
	{
		this.Simon = Simon;

		if(FahooeyGame.trueTheme)
		{
			text = new String[] 
					{
					"This game was made by a lunatic.",
					"",
					"Sounds stolen from:",
					"Soundbible.com; Soundjax.com; Civil Defense Alarm",
					"",
					"Songs:",
					"I Ain't Your Antichrist, I'm Your Uncle Sam",
					"by Gunther the Clown",
					"",
					"Boss Theme from Sonic CD (US version)",
					};
		}
		else
		{
			text = new String[] 
					{
					"This game was made by a lunatic.",
					"",
					"Sounds stolen from:",
					"Soundbible.com; Soundjax.com; Civil Defense Alarm",
					"",
					"Songs:",
					"Didgeridoo",
					"http://www.youtube.com/watch?v=zu4pWM-nRq8",
					"",
					"Boss Theme from Sonic CD (US version)",
					};
		}
	}
	public void Show()
	{
		Simon.fill(0);
		Simon.textSize(24);
		Simon.textAlign(Simon.CENTER);

		for(int i = text.length - 1; i >= 0; i--)
		{
			Simon.text(text[i], Simon.width / 2, 50 + (30 * i));
		}

		if(Simon.mouseX > 20 && Simon.mouseX < 20 + 300)
		{
			if(Simon.mouseY > Simon.height - 20 - 50 && Simon.mouseY < Simon.height - 20)
			{
				Simon.fill(30, 255, 255);
			}
			else
			{
				Simon.fill(255);
			}
		}
		else
		{
			Simon.fill(255);
		}
		Simon.rect(20, Simon.height - 20, 300, -50);
		Simon.fill(0);
		Simon.text("Back to main menu", 160, Simon.height - 40);
	}
	/**
	 * Tells game whether or not to goes back to main menu.
	 * @return True or false.
	 */
	public boolean ClickExit()
	{
		if(Simon.mouseX > 20 && Simon.mouseX < 20 + 300)
		{
			if(Simon.mouseY > Simon.height - 20 - 50 && Simon.mouseY < Simon.height - 20)
			{
				return true;
			}
		}
		return false;
	}
}
