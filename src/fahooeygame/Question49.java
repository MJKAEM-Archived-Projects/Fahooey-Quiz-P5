package fahooeygame;

import processing.core.PApplet;
import processing.core.PImage;

public class Question49 extends Question666
{
	private PImage Star;
	/**
	 * 
	 * @param Type 'this' or whatever PApplet is passed as.
	 * @param The question. Each position in the array moves it down.  Do not use the backslash n.
	 * @param The array of answers.
	 * @param Value of 0-3, whichever answer is correct. Any other indicates the boxes are all wrong.
	 */
	public Question49(PApplet Simon, String q[], String[] a)
	{
		super(Simon, q, a, -5);
		
		Star = Simon.loadImage("Star.png");
	}

	/**
	 * Returns a number indicating whether the buttons are clicked
	 * and whether or not it is correct.
	 * @return 0 for not clicked. 1 for wrong. 2 for correct.
	 */
	public byte ClickAnswers()
	{
		if(GetSimon().mouseX > 20 && GetSimon().mouseX < GetSimon().width - 20)
		{
			if(GetSimon().mouseY > GetSimon().height / 2 + 5 && 
					GetSimon().mouseY < GetSimon().height - 20)
			{
				return 2;
			}
		}
		return 0;
	}

	public void DisplayQuestion()
	{
		GetSimon().image(Star, (GetSimon().width / 2) - (Star.width / 2), (float) (GetSimon().height * .25) - (Star.height / 2));
		
		super.DisplayQuestion();
	}
	
	public void DisplayAnswers()
	{
		GetSimon().textFont(FahooeyGame.bigFont2);
		GetSimon().textSize(48);
		GetSimon().textAlign(GetSimon().CENTER);
		GetSimon().strokeWeight(1);

		//Upper Left Box (0)
		FillerBox(
				(GetSimon().mouseX > 20 && GetSimon().mouseX < GetSimon().width - 20),
				(GetSimon().mouseY > GetSimon().height / 2 + 5 && GetSimon().mouseY < GetSimon().height - 20)
				);

		GetSimon().rect(
				20, 
				GetSimon().height / 2 + 5, 
				GetSimon().width - 20 - 20, 
				(float)(GetSimon().height - 20 - (GetSimon().height / 2 + 5))
				);

		GetSimon().fill(255);
		GetSimon().text(GetAnswers(0).GetAnswer(), (int)((GetSimon().width * .5)), /*(int)(GetSimon().height / 2 + 5) + 50*/ (int)(GetSimon().height * .75));
	}

	
}
